#!/usr/bin/env bash

IMAGE=java-proxy
VERSION=latest
REGISTRY_HOST=quay.io/justindav1s

mvn clean package -Dspring.profiles.active=dev

cp target/java-proxy-0.0.1-SNAPSHOT.jar .

docker build -t $IMAGE .

rm -rf java-proxy-0.0.1-SNAPSHOT.jar

TAG=$REGISTRY_HOST/$IMAGE:$VERSION

docker tag $IMAGE $TAG

docker login -p $QUAYIO_PASSWORD -u $QUAYIO_USER $REGISTRY_HOST

docker push $TAG

#run it

#docker run -d -p 8080:8080 quay.io/justindav1s/java-proxy