package org.jnd.microservices.javaproxy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Value;
import javax.annotation.PostConstruct;

@Configuration
@EnableAutoConfiguration
@SpringBootApplication
@RestController
public class JavaProxyApplication extends SpringBootServletInitializer  {


    @Value( "${egress.url}" )
    String egress_url;

    public static final Logger log = LoggerFactory.getLogger(JavaProxyApplication.class);

    public static void main(String[] args) {

        SpringApplication.run(JavaProxyApplication.class, args);


    }

    @PostConstruct
    public void debug() {


        log.debug("Egress URL : "+egress_url);
    }

}
