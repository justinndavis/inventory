package org.jnd.microservices.javaproxy.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.client.RestTemplate;
import org.springframework.http.HttpStatus;

@CrossOrigin
@RestController
@RequestMapping("/java")
public class JavaProxyController {

    private static final Logger log = LoggerFactory.getLogger(JavaProxyController.class);

    @Value( "${inventory.host}" )
    String inventory_host;

    @Value( "${egress.url}" )
    String egress_url;

    private RestTemplate restTemplate = new RestTemplate();

    @RequestMapping(value = "/egress", method = RequestMethod.GET, produces = "application/json")
    ResponseEntity<?> egresstest(@RequestHeader HttpHeaders headers) {

        ResponseEntity<String> response
        = restTemplate.getForEntity(egress_url, String.class);

        log.debug("1 Egress Response : "+response.getBody());

        return new ResponseEntity<>(response.getBody(), null, HttpStatus.OK);

    }


    @RequestMapping(value = "/products/all", method = RequestMethod.GET, produces = "application/json")
    ResponseEntity<?> getAllProducts(@RequestHeader HttpHeaders headers) {

        String url = "http://"+inventory_host+"/products/all";
        
        ResponseEntity<String> response
            = restTemplate.getForEntity(url, String.class);

        log.debug("1 All Products Response : "+response.getBody());

        if (response == null)
            throw new RuntimeException();

        log.debug("2 All Products Response : "+response.getBody());
        return new ResponseEntity<>(response.getBody(), null, HttpStatus.OK);
    }

    @RequestMapping(value = "/products/types", method = RequestMethod.GET, produces = "application/json")
    ResponseEntity<?> getAllProductsTypes(@RequestHeader HttpHeaders headers) {

        String url = "http://"+inventory_host+"/products/types";
        
        ResponseEntity<String> response
            = restTemplate.getForEntity(url, String.class);

        log.debug("1 Products types Response : "+response.getBody());

        if (response == null)
            throw new RuntimeException();

        log.debug("2  Products types Response : "+response.getBody());

        return new ResponseEntity<>(response.getBody(), null, HttpStatus.OK);
    }

    @RequestMapping(value = "/products/health", method = RequestMethod.GET, produces = "application/json")
    ResponseEntity<?> getAllProductsHealth(@RequestHeader HttpHeaders headers) {

        String url = "http://"+inventory_host+"/health";
        
        ResponseEntity<String> response
            = restTemplate.getForEntity(url, String.class);

        log.debug("1 Products health Response : "+response.getBody());

        if (response == null)
            throw new RuntimeException();

        log.debug("2  Products health Response : "+response.getBody());
        return new ResponseEntity<>(response.getBody(), null, HttpStatus.OK);
    }
}
