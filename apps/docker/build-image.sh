#!/usr/bin/env bash

IMAGE=inventory
VERSION=latest
REGISTRY_HOST=quay.io/justindav1s


cd ../inventory
mvn clean package -Dspring.profiles.active=dev
cd -

cp ../inventory/target/inventory-0.0.1-SNAPSHOT.jar .

docker build -t $IMAGE .

rm -rf inventory-0.0.1-SNAPSHOT.jar

TAG=$REGISTRY_HOST/$IMAGE:$VERSION

docker tag $IMAGE $TAG

docker login -p $QUAYIO_PASSWORD -u $QUAYIO_USER $REGISTRY_HOST

docker push $TAG

#run it

docker run -d -p 8080:8080 quay.io/justindav1s/inventory