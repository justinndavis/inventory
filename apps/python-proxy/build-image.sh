#!/usr/bin/env bash

IMAGE=python-proxy
VERSION=latest
REGISTRY_HOST=$QUAYIO_HOST

docker build -t $IMAGE .

TAG=$REGISTRY_HOST/$QUAYIO_USER/$IMAGE:$VERSION

docker tag $IMAGE $TAG

docker login -p $QUAYIO_PASSWORD -u $QUAYIO_USER $REGISTRY_HOST


docker push $TAG

#run it

#docker run -d -p 8080:8080 $REGISTRY_HOST/$QUAYIO_USER/$IMAGE